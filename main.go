package main

import (
	"fmt"
	"fresco/slash"
	"net/http"
)

func main() {

	http.HandleFunc("/events-endpoint", slash.F)

	fmt.Println("[INFO] Server listening")
	http.ListenAndServe(":3000", nil)
}
