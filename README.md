# Fresco

```shell
gcloud config configurations activate everydev
```

```shell
cd lib
gcloud functions deploy fresco --runtime go111 --entry-point F --trigger-http
```

```shell
curl -G -d "email=robrowlandmusic@gmail.com" localhost:8080
```
