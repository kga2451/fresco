package new

import (
	"encoding/json"
	"net/http"
)

// FrescoPayload to receive message from fresco
type FrescoPayload struct {
	Message string
}

// F the google function
func F(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	var f FrescoPayload
	err := decoder.Decode(&f)
	if err != nil {
		panic(err)
	}

	w.Write([]byte("The clan at `new` bids you a good day.  Did you send: " + f.Message + "?"))

}
