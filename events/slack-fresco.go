package events

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
)

var api = slack.New("xoxb-2613082712-741522651748-cvNkP1PaX8vzcnHZ31LeYej7")

// F main entry point for GCP
func F(w http.ResponseWriter, r *http.Request) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	body := buf.String()

	eventsAPIEvent, e := slackevents.ParseEvent(json.RawMessage(body), slackevents.OptionVerifyToken(&slackevents.TokenComparator{VerificationToken: "4wzwDcAmGiaE3Ou3J5gUswK3"}))
	if e != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println("error 1")
		fmt.Printf("%s", e.Error())
		fmt.Printf("%v", string(json.RawMessage(body)))
	}

	if eventsAPIEvent.Type == slackevents.URLVerification {
		fmt.Println("urlVerification")
		var r *slackevents.ChallengeResponse
		err := json.Unmarshal([]byte(body), &r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Printf("%s", e.Error())

		}
		fmt.Printf("%v", body)

		w.Header().Set("Content-Type", "text")
		w.Write([]byte(r.Challenge))
	}

	if eventsAPIEvent.Type == slackevents.CallbackEvent {
		fmt.Println("callback")
		innerEvent := eventsAPIEvent.InnerEvent
		switch ev := innerEvent.Data.(type) {
		case *slackevents.AppMentionEvent:
			api.PostMessage(ev.Channel, slack.MsgOptionText("Yes, hello.", false))
		}
	}
}
