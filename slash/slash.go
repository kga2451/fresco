package slash

import (
	"fmt"
	"net/http"

	"github.com/slack-go/slack"
)

// F the google function
func F(w http.ResponseWriter, r *http.Request) {

	s, e := slack.SlashCommandParse(r)
	if e != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}

	c := Command{TeamDomain: s.TeamDomain, RawText: s.Text}
	c.Parse()
	responseText := c.Call()

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(responseText))

	fmt.Printf("%v", s)

}
