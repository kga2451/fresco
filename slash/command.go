package slash

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

// Command to contain Fresco commands
type Command struct {
	RawText    string
	TeamDomain string
	Action     string
	Rest       string
}

// Action contains the action struct
type Action struct {
	Keyword string
	Data    ActionData
}

// ActionData contains the data associated with an action
type ActionData struct {
	URL  string `json:"url"`
	Text string `json:"text"`
}

// Parse incoming text
func (c *Command) Parse() {

	w := strings.Split(c.RawText, " ")

	c.Action = strings.ToLower(w[0])
	c.Rest = strings.Join(w[1:], " ")
}

// Call the URL by command keyword
func (c *Command) Call() string {

	a := getActions(c.Action)

	if a.Text != "" {
		return a.Text
	}

	if isValidURL(a.URL) {
		r, err := http.Get(a.URL)
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return "error"
		}
		return string(body)

	}

	return "I don't understand what you're trying to do"
}

func getActions(keyword string) ActionData {

	var actions = make(map[string]ActionData)

	actionURL := "https://fresco-dev.storage.googleapis.com/actions/commands.json"

	r, err := http.Get(actionURL)
	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(r.Body)

	err = json.Unmarshal(body, &actions)
	if err != nil {
		fmt.Println("error:", err)
	}

	fmt.Printf("%s\n%v", keyword, actions)

	return actions[keyword]
}

func isValidURL(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	if err != nil {
		return false
	}

	u, err := url.Parse(toTest)
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	}

	return true
}
